import 'dart:math';

import 'package:flutter/material.dart';

import '../model/hospital_list_item.dart';

class ComponentListItem extends StatelessWidget {
  const ComponentListItem({super.key, required this.item, required this.callback});

  final HospitalListItem item;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.only(bottom: 10),
        decoration: BoxDecoration(
          color: Colors.white.withOpacity(.50),
          border: Border.all(width: 5, color: Colors.primaries[Random().nextInt(Colors.primaries.length)])
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text('${item.orgnm == null ? '병원 이름이 없습니다.' : item.orgnm}',
            style: TextStyle(
              color: Colors.black,
              fontSize: 18,
              fontWeight: FontWeight.bold,
              fontStyle: FontStyle.italic
            ),),
            Text('요양기관 번호 = ${item.orgcd == null ? '없습니다.' : item.orgcd}'),
            Text('전화 번호 = ${item.orgTlno == null ? '없습니다.' : item.orgTlno}'),
            Text('주소 = ${item.orgZipaddr == null ? '없습니다.' : item.orgZipaddr}'),
            Text(
                '점심 시간 = ${item.lunchSttTm == null ? '점심 시작 시간이 없습니다.' : item.lunchSttTm} : ${item.lunchEndTm == null ? '점심 종료 시간이 없습니다.' : item.lunchEndTm} '),
            Text('${item.slrYmd == null ? '없습니다.' : item.slrYmd}'),
          ],
        ),
      ),
    );
  }
}
