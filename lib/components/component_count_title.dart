import 'package:flutter/material.dart';

class ComponentCountTitle extends StatelessWidget {
  const ComponentCountTitle({super.key, required this.icon, required this.count, required this.unitName, required this.itemName});

  final IconData icon;
  final int count;
  final String unitName;
  final String itemName;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(icon, size: 20, color: Colors.pink,),
          const SizedBox(width: 10,),
          Text('총 ${count.toString()}$unitName의 $itemName이(가) 있습니다.',
          style: TextStyle(
            fontSize: 20,
            color: Colors.black,
            fontWeight: FontWeight.bold
          ),)
        ],
      ),
    );
  }
}
