import 'package:app_api_test1_dup/repository/repo_hospital_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import '../components/component_count_title.dart';
import '../components/component_list_item.dart';
import '../components/component_no_contents.dart';
import '../model/hospital_list_item.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  final _scrollController = ScrollController();

  final _formKey = GlobalKey<FormBuilderState>();
  bool _areaHasError = false;
  var areaOptions = ['전체', '서울특별시', '경기도 안산시', '경기도 시흥시', '인천광역시'];

  List<HospitalListItem> _list = [];
  int _page = 1; //현재 페이지
  int _totalPage = 1; //총 페이지 갯수. 이게 있어야 마지막 페이지가 몇번째인지 알 수 있다.
  //이걸 구하려면 matchCount를 perPage로 나누고, 올림을 해주면 된다. (데이터가 딱 떨어지는 경우도 있으므로)
  int _perPage = 10; //한 페이지당 보여줄 아이템 갯수
  int _totalCount = 0; //총 아이템 갯수
  int _currentCount = 0; //현재 보여지고 있는 아이템 번호
  int _matchCount = 0; //검색 결과 갯수
  String _searchArea = '전체';

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.offset == _scrollController.position.maxScrollExtent) {
        _loadItems();
      }
    });
    _loadItems();
  }

  Future<void> _loadItems({bool reFresh = false}) async {
    if (reFresh) {
      _list = [];
      _page = 1;
      _totalPage = 1;
      _perPage = 10;
      _totalCount = 0;
      _currentCount = 0;
      _matchCount = 0;
    }

    if (_page <= _totalPage) {
      await RepoHospitalList()
          .getList(page: _page, searchArea: _searchArea)
          .then((res) => {
        setState(() {
          _totalPage = (res.matchCount / res.perPage).ceil(); //매치카운트를 퍼페이지로 나누고.올림
          _totalCount = res.totalCount;
          _currentCount = res.currentCount;
          _matchCount = res.matchCount;

          _list = [..._list, ...?res.data];

          _page ++;
        })
      })
          .catchError((err) => {
        debugPrint(err)
      });
    }

    if (reFresh) {
      _scrollController.animateTo(0, duration: const Duration(milliseconds: 300), curve: Curves.easeOut);
    }
  }


  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/back6.jpg"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            title: const Text('api 테스트',
            style: TextStyle(color: Colors.pinkAccent,
            fontStyle: FontStyle.italic,
            fontWeight: FontWeight.bold,
            ),),
            backgroundColor: Colors.transparent,
            elevation: 0,
          ),
          body: ListView(
            controller: _scrollController,
            children: [
              _buildBody(),
            ],
          ),
          bottomNavigationBar: BottomAppBar(
            shape: CircularNotchedRectangle(),
            child: FormBuilder(
              key: _formKey,
              autovalidateMode: AutovalidateMode.disabled,
              child: FormBuilderDropdown<String>(
                //autovalidate: true,
                name: 'area',
                decoration: const InputDecoration(
                  labelText: '지역',
                  hintText: 'Select Area',
                ),
                items: areaOptions
                    .map((area) => DropdownMenuItem(
                  alignment: AlignmentDirectional.center,
                  value: area,
                  child: Text(area),
                ))
                    .toList(),
                onChanged: (val) {
                  setState(() {
                    _searchArea = _formKey.currentState!.fields['area']!.value;
                    _loadItems(reFresh: true);
                  });
                },
                valueTransformer: (val) => val?.toString(),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildBody() {
    if (_matchCount > 0) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ComponentCountTitle(icon: Icons.favorite, count: _matchCount, unitName: '건', itemName: '병원'),
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) => ComponentListItem(item: _list[index], callback: () {}),
          ),
        ],
      );

    } else {
      return SizedBox(
        height: MediaQuery.of(context).size.height - 50,
        child: const ComponentNoContents(icon: Icons.favorite, msg: '데이터가 없습니다'),
      );
    }
  }
}
