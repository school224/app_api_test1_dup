import 'package:app_api_test1_dup/config/config_api.dart';
import 'package:dio/dio.dart';

import '../model/hospital_list.dart';

class RepoHospitalList {
  final String _baseUrl = 'https://api.odcloud.kr/api/apnmOrg/v1/list?page={page}&perPage={perPage}&serviceKey=ffzbt251LaJ%2F7rHLJrCbVET2d8hg7FDVqSHWJ1HtP9TwtJF%2Fyr20ixpa73bbbQwgZYp8br1jyNhYF9aUc6rEGQ%3D%3D';

  Future<HospitalList> getList({int page = 1, int perPage = 10, String searchArea = '전체'}) async {
    String _resultUrl = _baseUrl.replaceAll('{page}', page.toString());
    _resultUrl = _resultUrl.replaceAll('{perPage}', perPage.toString());
    if (searchArea != '전체') {
      _resultUrl = _resultUrl + '&cond%5BorgZipaddr%3A%3ALIKE%5D=${Uri.encodeFull(searchArea)}';
    }

    Dio dio = Dio();

    final response = await dio.get(_resultUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              if (status == 200) {
                return true;
              } else {
                return false;
              }
            }));

    return HospitalList.fromJson(response.data);
  }
}